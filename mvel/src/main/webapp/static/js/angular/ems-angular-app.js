'use strict';

/**
 * @ngdoc overview
 * @name emsApp
 * @description # emsApp
 * 
 * Main module of the application.
 */
angular.module('emsApp', []).constant("GLOBAL", {
	API_BASE : "http://localhost:8080/ems",
	ANGULAR_BASE : "http://localhost:8080/ems",
}).run(function($http) {
	var csrfToken = window.csrf;
	if (csrfToken) {
		$http.defaults.headers.common['X-CSRF-TOKEN'] = csrfToken.value;
	}
});
