<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@include file="../common/_taglibs.jsp"%>
<html>
<head>
<base href="<c:url value="/"></c:url>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- <script type="text/javascript" src="static/js/angular/scripts/util/util.js"></script> -->
<tiles:insertAttribute name="styles"></tiles:insertAttribute>
<title><tiles:insertAttribute name="title"></tiles:insertAttribute></title>
<style type="text/css">
	.ng-cloak{
		display: none !important;
	}
</style>
   	<!-- <script type="text/javascript" src="static/plugins/html2canvas/html2canvas.min.js"></script> -->
	<!-- <script type="text/javascript" src="http://mrrio.github.io/jsPDF/dist/jspdf.debug.js"></script> -->
	<!-- <script type="text/javascript" src="static/js/angular/scripts/util/util.js"></script> -->
	
</head>
<body class="skin-blue sidebar-mini" data-ng-app="emsApp">
	<div class="wrapper">
		<tiles:insertAttribute name="header"></tiles:insertAttribute>
		<tiles:insertAttribute name="sidebar"></tiles:insertAttribute>
		<tiles:insertAttribute name="body"></tiles:insertAttribute>
		<tiles:insertAttribute name="footer"></tiles:insertAttribute>
	</div>
</body>
</html>