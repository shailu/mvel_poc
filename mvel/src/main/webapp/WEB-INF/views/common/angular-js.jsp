<script type="text/javascript" src="static/js/angular/angular.min.js"></script>
<script type="text/javascript" src="static/js/angular/ems-angular-app.js"></script>

<!-- Angular controllers JS -->
<script type="text/javascript" src="static/js/angular/scripts/controllers/xyz.js"></script>

<!-- Angular services JS -->
<script type="text/javascript" src="static/js/angular/scripts/services/xyz.js"></script>

<!-- Angular factories JS -->
<script type="text/javascript" src="static/js/angular/scripts/factory/http-interceptor.js"></script>
<script type="text/javascript" src="static/js/angular/scripts/factory/constants.js"></script>

<!-- Angular directives JS -->
<script type="text/javascript" src="static/js/angular/scripts/directive/xyz.js"></script>