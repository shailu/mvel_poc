/**
 * 
 */
package com.mvel.service;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mvel.helper.Employee;
import com.mvel.helper.Flight;

/**
 * The Class ActivitiTestService.
 *
 * @author shailendra singh
 */
@Service
public class ActivitiTestService {
	
	/** The Constant log. */
	private static final Logger log = Logger.getLogger(ActivitiTestService.class);
	
	/** The process engine. */
	@Autowired
	private ProcessEngine processEngine;
	
	@Autowired
	private RuntimeService runtimeService;

	/**
	 * Show activiti response.
	 */
	public Employee showActivitiResponse(Employee employee) {
		
		log.info("showActivitiResponse()");
		
		/*String pName = processEngine.getName();
	    String ver = ProcessEngine.VERSION;
	    log.info("ProcessEngine [" + pName + "] Version: [" + ver + "]");*/
		
	    Map<String, Object> input = new HashMap<>();
	    input.put("expenseCategory", "Flight");
	    input.put("employee", employee);
	    log.info("input :- "+input);
	    ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("firstProcess", input);
	    
	    Employee myResult = (Employee) runtimeService.getVariable(processInstance.getId(), "response");
	    
	    log.info("Process completed, myResult :- "+myResult);
	    
	    TaskService taskService = processEngine.getTaskService();
	    /*List<Task> tasks = taskService.createTaskQuery().taskCandidateGroup("management").list();
	    Task task = tasks.get(0);*/
	    
	    Task task = taskService.createTaskQuery().taskCandidateGroup("management").singleResult();
	    taskService.complete(task.getId());
	    
	    return myResult;
	    
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> searchFlights(Employee emp) {
		
		log.info("searchFlights() from :- "+emp);
		Map<String, Object> output = new HashMap<>();
	    Map<String, Object> input = new HashMap<>();
	    input.put("employee", emp);
	    log.info("input :- "+input);
	    ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("srpProcess", input);
	    
	    if(!processInstance.isEnded()) {
	    	List<Flight> flights = (List<Flight>) runtimeService.getVariable(processInstance.getId(), "filteredFlights");
		    
		    log.info("Filtered flights found, flight :- "+flights);
		    
		    TaskService taskService = processEngine.getTaskService();
		    /*List<Task> tasks = taskService.createTaskQuery().taskCandidateGroup("management").list();
		    Task task = tasks.get(0);*/
		    
		    Task task = taskService.createTaskQuery().processInstanceId(processInstance.getId()).taskCandidateGroup("waiting-to-book-selected-flight").singleResult();
		    //taskService.complete(task.getId());
		    output.put("status", "success");
		    output.put("flights", flights);
		    output.put("maxValue", emp.getMaxValue());
		    output.put("pid", processInstance.getId());
		    output.put("tid", task.getId());
		    
	    } else {
	    	output.put("status", "failure");
	    	output.put("message", "No flight found.");
	    }
	    return output;	    
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> bookFlight(Map<String, Object> map) {
		log.info("bookFlight() map :- "+map);
		Map<String, Object> output = new HashMap<>();
	    TaskService taskService = processEngine.getTaskService();
	    
	    String pid = (String)map.get("pid");
	    String tid = (String)map.get("tid");
	    
	    if(pid != null && !pid.isEmpty() && tid != null && !tid.isEmpty()) {
	    	TaskQuery taskQuery = taskService.createTaskQuery();
	    	Task taskExists = taskQuery.processInstanceId(pid).taskId(tid).singleResult();
		    if(taskExists != null) {
		    	LinkedHashMap<String, Object> employeeDetails = (LinkedHashMap<String, Object>) map.get("employee");
			    Employee emp = new Employee();
			    emp.setBand((String)employeeDetails.get("band"));
			    Flight fl = new Flight();
			    fl.setFrom((String)((LinkedHashMap<String, Object>)employeeDetails.get("flight")).get("from"));
			    fl.setTo((String)((LinkedHashMap<String, Object>)employeeDetails.get("flight")).get("to"));
			    emp.setFlight(fl);
			    map.put("employee", emp);
			    
			    taskService.complete(tid, map);
			    
			    ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(pid).singleResult();
			    if(processInstance != null && !processInstance.isEnded()) {
			    	/*String approverList = (String) runtimeService.getVariable(pid, "approverList");
				    log.info("approverList :- "+approverList);
			    	
				    Task selectFlightTask = taskQuery.processInstanceId(pid).taskCandidateGroup("waiting-to-book-selected-flight").singleResult();*/
				    
			    	String approvalRequestStatus = (String) runtimeService.getVariable(pid, "requestStatus");
				    log.info("approvalRequestStatus :- "+approvalRequestStatus);
				    
				    Task approvalWaitTask = taskQuery.processInstanceId(pid).taskCandidateGroup("waiting-to-get-approval").singleResult();
				    output.put("status", "success");
				    output.put("approvalRequestStatus", approvalRequestStatus);
				    output.put("pid", pid);
				    output.put("tid", approvalWaitTask.getId());
			    } else {
			    	output.put("pid", null);
				    output.put("tid", null);
			    	output.put("status", "failure");
			    	output.put("message", "No approver found to approve this band.");
			    }
			    
			    
		    } else {
		    	output.put("status", "failure");
			    output.put("approvalRequestStatus", null);
			    output.put("pid", null);
			    output.put("tid", null);
			    output.put("message", "Process has already been moved. Search again for another booking.");
		    }
	    } else {
	    	output.put("status", "failure");
		    output.put("message", "PID & TID not found in request.");
	    }
	    
	    return output;
	    
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> approvedNowGoForBookFlight(Map<String, Object> map) {
		log.info("approvedNowGoForBookFlight() map :- "+map);
	    TaskService taskService = processEngine.getTaskService();
	    
	    String pid = (String)map.get("pid");
	    String tid = (String)map.get("tid");
	    
	    if(pid != null && !pid.isEmpty() && tid != null && !tid.isEmpty()) {
	    	Task taskExists = taskService.createTaskQuery().processInstanceId(pid).taskId(tid).singleResult();
		    if(taskExists != null) {
		    	LinkedHashMap<String, Object> employeeDetails = (LinkedHashMap<String, Object>) map.get("employee");
			    Employee emp = new Employee();
			    emp.setBand((String)employeeDetails.get("band"));
			    Flight empFlight = new Flight();
			    empFlight.setFrom((String)((LinkedHashMap<String, Object>)employeeDetails.get("flight")).get("from"));
			    empFlight.setTo((String)((LinkedHashMap<String, Object>)employeeDetails.get("flight")).get("to"));
			    emp.setFlight(empFlight);
			    map.put("employee", emp);
			    
			    LinkedHashMap<String, Object> flightDetails = (LinkedHashMap<String, Object>) map.get("flight");
			    Flight sysFlight = new Flight();
			    sysFlight.setFrom((String)(flightDetails.get("from")));
			    sysFlight.setTo((String)(flightDetails.get("to")));
			    sysFlight.setDirectFlight((boolean)(flightDetails.get("directFlight")));
			    sysFlight.setPrice((int)(flightDetails.get("price")));
			    map.put("flight", sysFlight);
			    
			    taskService.complete(tid, map);
			    
			    String bookingStatus = (String) runtimeService.getVariable(pid, "bookingStatus");
			    log.info("bookingStatus :- "+bookingStatus);
			    
			    Task task = taskService.createTaskQuery().processInstanceId(pid).taskCandidateGroup("waiting-to-get-booking-status").singleResult();
			    taskService.complete(task.getId());
			    
			    map.put("pid", null);
			    map.put("tid", null);
			    map.put("status", bookingStatus);
			    map.put("message", "Flight booked successfully.");
		    } else {
		    	map.put("pid", null);
			    map.put("tid", null);
			    map.put("status", "failure");
			    map.put("message", "This task does not exist any more. Process already moved.");
		    }
	    } else {
	    	map.put("status", "failure");
		    map.put("message", "PID & TID not found in request.");
	    }
	    
	    return map;
	}
	
	
	
}
