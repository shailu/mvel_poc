/**
 * 
 */
package com.mvel.service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.mvel2.MVEL;
import org.mvel2.util.ParseTools;
import org.springframework.stereotype.Service;

import com.mvel.helper.BandDef;
import com.mvel.helper.Employee;

/**
 * The Class EvelTestService.
 *
 * @author shailendra singh
 */
@Service("evelTestService")
public class EvelTestService {
	
	/** The Constant log. */
	private static final Logger log = Logger.getLogger(EvelTestService.class);
	
	//public static List<Employee> employeeList = populateEmployee();
	
	/*private static List<Employee> populateEmployee() {
        List<Employee> employees = new ArrayList<Employee>();
        Employee e = new Employee();
        e.setFirstName("john");
        e.setLastName("michale");
 
        Employee e1 = new Employee();
        e1.setFirstName("merlin");
        e1.setLastName("michale");
 
        employees.add(e);
        employees.add(e1);
        return employees;
    }*/
	
	/*public void showResponse() {
		// Here input to the MVEL expression is a map.
        Map<String, Object> input = new HashMap<String, Object>();
        Employee e = new Employee();
        e.setFirstName("john");
        e.setLastName("michale");
        input.put("employee", e);
        
         * Property Expression - Used to extract the property value out of the expression.
         
        String lastName = MVEL.evalToString("employee.lastName", input);
        log.info("Employee Last Name:" + lastName);
        
         * Boolean Expression
         
        log.info("Is employee name is John:" + MVEL.evalToBoolean("employee.lastName == \"john\"", input));
        input.put("numeric", new Double(-0.253405));
        log.info(MVEL.evalToBoolean("numeric > 0", input));
        input.put("input", employeeList);
        log.info(MVEL.eval("($ in input if $.firstName == \"john\")",input ));
        
        try {
        	
        	ClassLoader classLoader = getClass().getClassLoader();
        	File file = new File(classLoader.getResource("concat.mvel").getFile());
        	
            log.info(MVEL.eval(new String(ParseTools.loadFromFile(file)), input));
        } catch(IOException ex) {
        	ex.printStackTrace();
        }
	}*/
	
	/**
	 * Show rule based response.
	 */
	@SuppressWarnings("unchecked")
	public Employee showRuleBasedResponse(Employee employee) {
		
		try {
			ClassLoader classLoader = getClass().getClassLoader();
	    	File rule1 = new File(classLoader.getResource("rule_1.mvel").getFile());
	    	
	    	List<BandDef> listInput = new ArrayList<>();
			
			BandDef def1 = new BandDef();
			def1.setBand("Band-A");
			def1.setFrom("DEL");
			def1.setTo("BOM");
			def1.setOutput("MAX-5000");
	        listInput.add(def1);
	        
	        BandDef def2 = new BandDef();
	        def2.setBand("Band-B");
	        def2.setFrom("DEL");
	        def2.setTo("BOM");
	        def2.setOutput("MAX-8000");
	        listInput.add(def2);
	        
	        BandDef def3 = new BandDef();
	        def3.setBand("Band-A");
	        def3.setFrom("DEL");
	        def3.setTo("GOA");
	        def3.setOutput("MAX-8000");
	        listInput.add(def3);
	        
	        BandDef def4 = new BandDef();
			def4.setBand("Band-B");
			def4.setFrom("DEL");
			def4.setTo("GOA");
			def4.setOutput("MAX-10000");
	        listInput.add(def4);
	        
	        Map<String, Object> input = new HashMap<String, Object>();
	        input.put("bandDefList", listInput);
	        input.put("employee", employee);
	        
	        //Serializable compiled = MVEL.compileExpression("($.band + $.lastName in bandDefList)");
	        
	        //Object objPune = MVEL.eval(new String(ParseTools.loadFromFile(puneFile)), input);
	        //Object objNagpur = MVEL.eval(new String(ParseTools.loadFromFile(nagpurFile)), listInput);
	        
	        List<BandDef> outputList = (List<BandDef>) MVEL.eval(new String(ParseTools.loadFromFile(rule1)), input);
	        
	        if(outputList != null && !outputList.isEmpty()) {
	        	for (BandDef bandDef : outputList) {
	        		log.info(bandDef.getOutput());
	        		employee.setMaxValue(bandDef.getOutput());
				}
	        } else {
	        	log.info("No band matched with this employee.");
	        }
	        
		} catch(Exception e) {
			e.printStackTrace();
		}
		return employee;
		
	}
	
	/*public void showOutput() {
		
		String operator = ">";
		List<Double> computedValue = new ArrayList<Double>();
		computedValue.add(new Double(50.0));
		computedValue.add(new Double(97.9));
		computedValue.add(new Double(68.9));
		 
		Map<String, Object> input = new HashMap<String, Object>();
		input.put("actual_value", new Double(55.9));
		input.put("computed_value", computedValue);
		List output = null;
		 
		if (operator.equals(">")) {
            output = (List) MVEL.eval("($ in computed_value if $ > actual_value)", input);
        } else {
            output = (List) MVEL.eval("($ in computed_value if $ < actual_value)", input);
        }
		log.info(output);
	}*/

}
