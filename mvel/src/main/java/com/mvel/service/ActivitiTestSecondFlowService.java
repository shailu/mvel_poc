/**
 * 
 */
package com.mvel.service;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 * @author shailendra singh
 *
 */
@Service("activitiSecondFlowService")
public class ActivitiTestSecondFlowService {
	
	private static final Logger log = Logger.getLogger(ActivitiTestSecondFlowService.class);
	
	public String printMessage() {
		log.info("Employee has spent money in Accomodation (Hotel)");
		return "Max Value :- 3000";
	}

}
