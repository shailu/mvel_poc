/**
 * 
 */
package com.mvel.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.mvel.helper.Employee;

/**
 * @author shailendra singh
 *
 */
public class ActivitiFlightHandler {
	
	@Autowired
	private MvelTestService evelService;
	
	private static final Logger log = Logger.getLogger(ActivitiFlightHandler.class);
	
	public Employee processFlightRequest(Employee employee) {
		log.info("Employee has to spent money in Travel (Flight).");
        employee = evelService.getMaxValueOfEmployeeByRule(employee);
		return employee;
	}

}
