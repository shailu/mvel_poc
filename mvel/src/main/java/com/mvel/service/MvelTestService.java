/**
 * 
 */
package com.mvel.service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.mvel2.MVEL;
import org.mvel2.util.ParseTools;
import org.springframework.stereotype.Service;

import com.mvel.helper.BandDef;
import com.mvel.helper.Employee;
import com.mvel.helper.Flight;
import com.mvel.helper.Hotel;

/**
 * The Class EvelTestService.
 *
 * @author shailendra singh
 */
@Service("mvelTestService")
public class MvelTestService {
	
	/** The Constant log. */
	private static final Logger log = Logger.getLogger(MvelTestService.class);
	
	//public static List<Employee> employeeList = populateEmployee();
	//ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000);
	/*private static List<Employee> populateEmployee() {
        List<Employee> employees = new ArrayList<Employee>();
        Employee e = new Employee();
        e.setFirstName("john");
        e.setLastName("michale");
 
        Employee e1 = new Employee();
        e1.setFirstName("merlin");
        e1.setLastName("michale");
 
        employees.add(e);
        employees.add(e1);
        return employees;
    }*/
	
	/*public void showResponse() {
		// Here input to the MVEL expression is a map.
        Map<String, Object> input = new HashMap<String, Object>();
        Employee e = new Employee();
        e.setFirstName("john");
        e.setLastName("michale");
        input.put("employee", e);
        
         * Property Expression - Used to extract the property value out of the expression.
         
        String lastName = MVEL.evalToString("employee.lastName", input);
        log.info("Employee Last Name:" + lastName);
        
         * Boolean Expression
         
        log.info("Is employee name is John:" + MVEL.evalToBoolean("employee.lastName == \"john\"", input));
        input.put("numeric", new Double(-0.253405));
        log.info(MVEL.evalToBoolean("numeric > 0", input));
        input.put("input", employeeList);
        log.info(MVEL.eval("($ in input if $.firstName == \"john\")",input ));
        
        try {
        	
        	ClassLoader classLoader = getClass().getClassLoader();
        	File file = new File(classLoader.getResource("concat.mvel").getFile());
        	
            log.info(MVEL.eval(new String(ParseTools.loadFromFile(file)), input));
        } catch(IOException ex) {
        	ex.printStackTrace();
        }
	}*/
	
	/**
	 * Show rule based response.
	 */
	@SuppressWarnings("unchecked")
	public Employee getMaxValueOfEmployeeByRule(Employee employee) {
		
		try {
			ClassLoader classLoader = getClass().getClassLoader();
			File rule = null;
			if(employee.getFlight() != null) {
				rule = new File(classLoader.getResource("rule_1.mvel").getFile());
			} else {
				rule = new File(classLoader.getResource("rule_2.mvel").getFile());
			}
	    	
	    	List<BandDef> bandDefList = getBandDefForEmployee();
	        
	        Map<String, Object> input = new HashMap<String, Object>();
	        input.put("bandDefList", bandDefList);
	        input.put("employee", employee);
	        
	        //Serializable compiled = MVEL.compileExpression("($.band + $.lastName in bandDefList)");
	        
	        //Object objPune = MVEL.eval(new String(ParseTools.loadFromFile(puneFile)), input);
	        //Object objNagpur = MVEL.eval(new String(ParseTools.loadFromFile(nagpurFile)), listInput);
	        
	        List<BandDef> outputList = (List<BandDef>) MVEL.eval(new String(ParseTools.loadFromFile(rule)), input);
	        
	        if(outputList != null && !outputList.isEmpty()) {
	        	for (BandDef bandDef : outputList) {
	        		log.info(bandDef.getFlightMaxValue());
	        		if(employee.getFlight() != null) {
	        			employee.setMaxValue(bandDef.getFlightMaxValue());
	        		}
	        		if(employee.getHotel() != null) {
	        			employee.setMaxValue(bandDef.getHotelMaxValue());
	        		}
				}
	        } else {
	        	log.info("No band matched with this employee.");
	        }
	        
		} catch(Exception e) {
			e.printStackTrace();
		}
		return employee;
		
	}

	private List<BandDef> getBandDefForEmployee() {
		List<BandDef> listInput = new ArrayList<>();
		
		BandDef def1 = new BandDef();
		def1.setBand("Band-A");
		Flight flight = new Flight();
		flight.setFrom("DEL");
		flight.setTo("BOM");
		def1.setFlight(flight);
		def1.setFlightMaxValue(5500);
		Hotel hotel = new Hotel();
		hotel.setCity("BOM");
		hotel.setCountry("INDIA");
		hotel.setName("Taj Hotel");
		def1.setHotel(hotel);
		def1.setHotelMaxValue(3000);
		listInput.add(def1);
		
		BandDef def2 = new BandDef();
		def2.setBand("Band-B");
		Flight flight2 = new Flight();
		flight2.setFrom("DEL");
		flight2.setTo("BOM");
		def2.setFlight(flight2);
		def2.setFlightMaxValue(6000);
		Hotel hotel2 = new Hotel();
		hotel2.setCity("BOM");
		hotel2.setCountry("INDIA");
		hotel2.setName("Taj Hotel");
		def2.setHotel(hotel2);
		def2.setHotelMaxValue(5000);
		listInput.add(def2);
		
		BandDef def3 = new BandDef();
		def3.setBand("Band-A");
		Flight flight3 = new Flight();
		flight3.setFrom("DEL");
		flight3.setTo("GOA");
		def3.setFlight(flight3);
		def3.setFlightMaxValue(9000);
		Hotel hotel3 = new Hotel();
		hotel3.setCity("GOA");
		hotel3.setCountry("INDIA");
		hotel3.setName("Estrela");
		def3.setHotel(hotel3);
		def3.setHotelMaxValue(4000);
		listInput.add(def3);
		
		BandDef def4 = new BandDef();
		def4.setBand("Band-B");
		Flight flight4 = new Flight();
		flight4.setFrom("DEL");
		flight4.setTo("GOA");
		def4.setFlight(flight4);
		def4.setFlightMaxValue(10000);
		Hotel hotel4 = new Hotel();
		hotel4.setCity("GOA");
		hotel4.setCountry("INDIA");
		hotel4.setName("Estrela");
		def4.setHotel(hotel4);
		def4.setHotelMaxValue(6000);
		listInput.add(def4);
		return listInput;
	}
	
	@SuppressWarnings("unchecked")
	public List<Flight> getFlightsByEmployeeDetails(Employee employee) {
		
		try {
			ClassLoader classLoader = getClass().getClassLoader();
			File rule = null;
			if(employee.getFlight() != null) {
				rule = new File(classLoader.getResource("get_flights_by_rule.mvel").getFile());
			}
	    	
			List<Flight> flights = getFlights();
			
	        Map<String, Object> input = new HashMap<String, Object>();
	        input.put("flights", flights);
	        input.put("employee", employee);
	        
	        List<Flight> outputList = (List<Flight>) MVEL.eval(new String(ParseTools.loadFromFile(rule)), input);
	        
	        if(outputList != null && !outputList.isEmpty()) {
	        	return outputList;
	        } else {
	        	log.info("No flight was found.");
	        	return null;
	        }
	        
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		
		
	}

	private List<Flight> getFlights() {
		List<Flight> flights = new ArrayList<>();
		
		Flight flight1 = new Flight();
		flight1.setFrom("DEL");
		flight1.setTo("BOM");
		flight1.setPrice(5000);
		flights.add(flight1);
		
		Flight flight2 = new Flight();
		flight2.setDirectFlight(true);
		flight2.setFrom("DEL");
		flight2.setTo("BOM");
		flight2.setPrice(5500);
		flights.add(flight2);
		
		Flight flight3 = new Flight();
		flight3.setDirectFlight(true);
		flight3.setFrom("DEL");
		flight3.setTo("BOM");
		flight3.setPrice(6000);
		flights.add(flight3);
		
		Flight flight4 = new Flight();
		flight4.setFrom("DEL");
		flight4.setTo("GOA");
		flight4.setPrice(8000);
		flights.add(flight4);
		
		Flight flight5 = new Flight();
		flight5.setFrom("DEL");
		flight5.setTo("GOA");
		flight5.setPrice(8500);
		flights.add(flight5);
		
		Flight flight6 = new Flight();
		flight6.setDirectFlight(true);
		flight6.setFrom("DEL");
		flight6.setTo("GOA");
		flight6.setPrice(9000);
		flights.add(flight6);
		
		return flights;
	}

	@SuppressWarnings("unchecked")
	public List<Flight> getFilteredFlightsByEmployeeDetails(Employee employee, List<Flight> foundFlights) {
		
		try {
			ClassLoader classLoader = getClass().getClassLoader();
			File rule = null;
			if(employee.getFlight() != null) {
				rule = new File(classLoader.getResource("get_filtered_flights_by_rule.mvel").getFile());
			}
	    	
			employee = this.getMaxValueOfEmployeeByRule(employee);
			
	        Map<String, Object> input = new HashMap<String, Object>();
	        input.put("flights", foundFlights);
	        input.put("employee", employee);
	        
	        List<Flight> outputList = (List<Flight>) MVEL.eval(new String(ParseTools.loadFromFile(rule)), input);
	        
	        if(outputList != null && !outputList.isEmpty()) {
	        	return outputList;
	        } else {
	        	log.info("No flight was found after filtering.");
	        	return null;
	        }
	        
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		
		
	}
	
	/*public void showOutput() {
		
		String operator = ">";
		List<Double> computedValue = new ArrayList<Double>();
		computedValue.add(new Double(50.0));
		computedValue.add(new Double(97.9));
		computedValue.add(new Double(68.9));
		 
		Map<String, Object> input = new HashMap<String, Object>();
		input.put("actual_value", new Double(55.9));
		input.put("computed_value", computedValue);
		List output = null;
		 
		if (operator.equals(">")) {
            output = (List) MVEL.eval("($ in computed_value if $ > actual_value)", input);
        } else {
            output = (List) MVEL.eval("($ in computed_value if $ < actual_value)", input);
        }
		log.info(output);
	}*/

}
