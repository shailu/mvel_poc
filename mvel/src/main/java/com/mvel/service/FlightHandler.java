/**
 * 
 */
package com.mvel.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.mvel.helper.Employee;
import com.mvel.helper.Flight;

/**
 * The Class FlightHandler.
 *
 * @author shailendra singh
 */
public class FlightHandler {
	
	/** The evel service. */
	@Autowired
	private MvelTestService mvelService;
	
	/** The Constant log. */
	private static final Logger log = Logger.getLogger(FlightHandler.class);
	
	public List<Flight> getFlights(Employee emp) {
		log.info("getFlights() :- emp = "+emp);
		List<Flight> flights = mvelService.getFlightsByEmployeeDetails(emp);
		return flights;
	}
	
	public List<Flight> filterFlights(Employee emp, List<Flight> foundFlights) {
		log.info("filterFlights()");
		List<Flight> filteredFlights = mvelService.getFilteredFlightsByEmployeeDetails(emp, foundFlights);
		return filteredFlights;
	}
	
	public String getApproverList(Employee employee) {
		log.info("getApproverList()");
		if(employee != null) {
			if(employee.getBand().equalsIgnoreCase("Band-A")) {
				log.info("Band A matched - Approvers = S1, S2, S3");
				return "S1,S2,S3";
			} else if(employee.getBand().equalsIgnoreCase("Band-B")) {
				log.info("Band B matched - Approver = S3");
				return "S3";
			} else {
				log.info("Unknown Band of user/employee. No approver found.");
				return null;
			}
		} else {
			log.info("No employee given to match.");
			return null;
		}
	}
	
	public String sendApprovalRequest(String approverList) {
		log.info("Sending approval request to : "+approverList);
		log.info("approval-request-sent");
		return "approval-request-sent";
	}
	
	public String gotApprovalBookFlightNow(Employee employee, Flight flight) {
		log.info("gotApprovalBookFlightNow()");
		log.info("Booking flight...");
		log.info(employee.toString());
		log.info(flight.toString());
		log.info("Flight successfully booked.");
		return "success";
	}
	
	public String gotApprovalBookFlightNow() {
		log.info("gotApprovalBookFlightNow()");
		log.info("Booking flight...");
		log.info("Flight successfully booked.");
		return "success";
	}

}
