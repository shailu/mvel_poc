/**
 * 
 */
package com.mvel.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.mvel.helper.Employee;

/**
 * @author shailendra singh
 *
 */
public class ActivitiHotelHandler {
	
	private static final Logger log = Logger.getLogger(ActivitiHotelHandler.class);
	
	@Autowired
	private MvelTestService evelService;
	
	public Employee processHotelRequest(Employee employee) {
		log.info("Employee has to spent money in Accomodation (Flight).");
        employee = evelService.getMaxValueOfEmployeeByRule(employee);
		return employee;
	}

}
