/**
 * 
 */
package com.mvel.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mvel.helper.Employee;

/**
 * @author shailendra singh
 *
 */
@Service("activitiFirstFlowService")
public class ActivitiTestFirstFlowService {
	
	@Autowired
	private EvelTestService evelService;
	
	private static final Logger log = Logger.getLogger(ActivitiTestFirstFlowService.class);
	
	public Employee printMessage(Employee employee) {
		log.info("Employee has to spent money in Travel (Flight).");
		
        employee = evelService.showRuleBasedResponse(employee);
		
		return employee;
	}

}
