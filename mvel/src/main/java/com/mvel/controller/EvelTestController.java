/**
 * 
 */
package com.mvel.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mvel.service.EvelTestService;

/**
 * The Class EvelTestController.
 *
 * @author shailendra singh
 */
@Controller
public class EvelTestController {
	
	/** The Constant log. */
	private static final Logger log = Logger.getLogger(EvelTestController.class);
	
	@Autowired
	private EvelTestService evelService;
	
	/**
	 * Gets the external users page.
	 *
	 * @param name the name
	 * @param model the model
	 * @return the external users page
	 */
	@RequestMapping(value = "/test-mvel.htm")
	public String getExternalUsersPage(@RequestParam(value = "name", required = false, defaultValue = "World") String name, Model model) {
		log.info("In Evel Test controller");
		model.addAttribute("message", "Welcome to MVEL.");
		model.addAttribute("name", name);
		
		//evelService.showRuleBasedResponse();
		
		//evelService.showOutput();
		
		return "test";
	}
	
}
