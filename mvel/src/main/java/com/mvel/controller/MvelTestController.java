/**
 * 
 */
package com.mvel.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mvel.service.MvelTestService;

/**
 * The Class MvelTestController.
 *
 * @author shailendra singh
 */
@Controller
public class MvelTestController {
	
	/** The Constant log. */
	private static final Logger log = Logger.getLogger(MvelTestController.class);
	
	@Autowired
	private MvelTestService mvelService;
	
	/**
	 * Gets the external users page.
	 *
	 * @param name the name
	 * @param model the model
	 * @return the external users page
	 */
	@RequestMapping(value = "/test-mvel.htm")
	public String getExternalUsersPage(@RequestParam(value = "name", required = false, defaultValue = "World") String name, Model model) {
		log.info("In Evel Test controller");
		model.addAttribute("message", "Welcome to MVEL.");
		model.addAttribute("name", name);
		
		//evelService.showRuleBasedResponse();
		
		//evelService.showOutput();
		
		return "test";
	}
	
}
