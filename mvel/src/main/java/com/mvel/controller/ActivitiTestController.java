/**
 * 
 */
package com.mvel.controller;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mvel.helper.Employee;
import com.mvel.service.ActivitiTestService;

/**
 * The Class ActivitiTestController.
 *
 * @author shailendra singh
 */
@RestController
public class ActivitiTestController {
	
	/** The Constant log. */
	private static final Logger log = Logger.getLogger(ActivitiTestController.class);
	
	@Autowired
	private ActivitiTestService activitiService;
	
	/**
	 * Gets the external users page.
	 *
	 * @param name the name
	 * @param model the model
	 * @return the external users page
	 */
	@RequestMapping(value = "/test-activiti.htm", method=RequestMethod.POST)
	@ResponseBody
	public Employee getExternalUsersPage(@RequestBody Employee emp) {
		log.info("In Activiti Test controller");
		/*Employee emp = new Employee();
		emp.setBand("Band-A");
		Flight flight = new Flight();
		flight.setFrom("DEL");
		flight.setTo("BOM");
		emp.setFlight(flight);*/
		
		activitiService.showActivitiResponse(emp);
		
		return emp;
	}
	
	@RequestMapping(value = "/search-flights.htm", method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getFlightFrom(@RequestBody Employee emp) {
		log.info("getFlightFrom()");
		Map<String, Object> map =  activitiService.searchFlights(emp);
		log.info("map :- "+map);
		return map;
	}
	
	@RequestMapping(value = "/book-flight.htm", method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> selectFlight(@RequestBody Map<String, Object> map) {
		log.info("selectFlight()");
		Map<String, Object> outputMap =  activitiService.bookFlight(map);
		log.info("outputMap :- "+outputMap);
		return outputMap;
	}
	
	@RequestMapping(value = "/approved-book-flight.htm", method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> bookFlight(@RequestBody Map<String, Object> map) {
		log.info("bookFlight()");
		Map<String, Object> status =  activitiService.approvedNowGoForBookFlight(map);
		return status;
	}
	
}
