/**
 * 
 */
package com.mvel.helper;

import java.io.Serializable;

/**
 * The Class Employee.
 *
 * @author shailendra singh
 */
public class Employee implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5912318413869912760L;

	/** The band. */
	private String band;
    
	/** The flight. */
	private Flight flight;
	
	/** The hotel. */
	private Hotel hotel;
	
	/** The max value. */
	private int maxValue;

	/**
	 * Gets the band.
	 *
	 * @return the band
	 */
	public String getBand() {
		return band;
	}

	/**
	 * Sets the band.
	 *
	 * @param band the new band
	 */
	public void setBand(String band) {
		this.band = band;
	}

	/**
	 * Gets the flight.
	 *
	 * @return the flight
	 */
	public Flight getFlight() {
		return flight;
	}

	/**
	 * Sets the flight.
	 *
	 * @param flight the new flight
	 */
	public void setFlight(Flight flight) {
		this.flight = flight;
	}

	/**
	 * Gets the hotel.
	 *
	 * @return the hotel
	 */
	public Hotel getHotel() {
		return hotel;
	}

	/**
	 * Sets the hotel.
	 *
	 * @param hotel the new hotel
	 */
	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	/**
	 * Gets the max value.
	 *
	 * @return the max value
	 */
	public int getMaxValue() {
		return maxValue;
	}

	/**
	 * Sets the max value.
	 *
	 * @param maxValue the new max value
	 */
	public void setMaxValue(int maxValue) {
		this.maxValue = maxValue;
	}

	@Override
	public String toString() {
		return "Employee [band=" + band + ", flight=" + flight + ", hotel=" + hotel + ", maxValue=" + maxValue + "]";
	}

}
