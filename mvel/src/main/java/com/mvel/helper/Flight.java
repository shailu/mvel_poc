/**
 * 
 */
package com.mvel.helper;

import java.io.Serializable;

/**
 * The Class Flight.
 *
 * @author shailendra singh
 */
public class Flight implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -534043757592793210L;

	/** The from. */
	private String from;
    
	/** The to. */
	private String to;
	
	/** The price. */
	private int price;
	
	/** The direct flight. */
	private boolean directFlight;

	/**
	 * Gets the from.
	 *
	 * @return the from
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * Sets the from.
	 *
	 * @param from the new from
	 */
	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 * Gets the to.
	 *
	 * @return the to
	 */
	public String getTo() {
		return to;
	}

	/**
	 * Sets the to.
	 *
	 * @param to the new to
	 */
	public void setTo(String to) {
		this.to = to;
	}

	/**
	 * Gets the price.
	 *
	 * @return the price
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * Sets the price.
	 *
	 * @param price the new price
	 */
	public void setPrice(int price) {
		this.price = price;
	}

	/**
	 * Gets the serialversionuid.
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * Checks if is direct flight.
	 *
	 * @return true, if is direct flight
	 */
	public boolean isDirectFlight() {
		return directFlight;
	}

	/**
	 * Sets the direct flight.
	 *
	 * @param directFlight the new direct flight
	 */
	public void setDirectFlight(boolean directFlight) {
		this.directFlight = directFlight;
	}

	@Override
	public String toString() {
		return "Flight [from=" + from + ", to=" + to + ", price=" + price + ", directFlight=" + directFlight + "]";
	}

}
