/**
 * 
 */
package com.mvel.helper;

import java.math.BigDecimal;

/**
 * The Class ItemCity.
 *
 * @author shailendra singh
 */
public class ItemCity {

	/**
	 * The Enum City.
	 */
	public enum City {
		/** The pune. */
		PUNE, 
		/** The nagpur. */
		NAGPUR
	}

	/**
	 * The Enum Type.
	 */
	public enum Type {
		/** The groceries. */
		GROCERIES, 
		/** The medicines. */
		MEDICINES, 
		/** The watches. */
		WATCHES, 
		/** The luxurygoods. */
		LUXURYGOODS
	}

	/** The purchase city. */
	private City purchaseCity;
	
	/** The sell price. */
	private BigDecimal sellPrice;
	
	/** The typeof item. */
	private Type typeofItem;
	
	/** The local tax. */
	private BigDecimal localTax;

	/**
	 * Gets the purchase city.
	 *
	 * @return the purchase city
	 */
	public City getPurchaseCity() {
		return purchaseCity;
	}

	/**
	 * Sets the purchase city.
	 *
	 * @param purchaseCity the new purchase city
	 */
	public void setPurchaseCity(City purchaseCity) {
		this.purchaseCity = purchaseCity;
	}

	/**
	 * Gets the sell price.
	 *
	 * @return the sell price
	 */
	public BigDecimal getSellPrice() {
		return sellPrice;
	}

	/**
	 * Sets the sell price.
	 *
	 * @param sellPrice the new sell price
	 */
	public void setSellPrice(BigDecimal sellPrice) {
		this.sellPrice = sellPrice;
	}

	/**
	 * Gets the typeof item.
	 *
	 * @return the typeof item
	 */
	public Type getTypeofItem() {
		return typeofItem;
	}

	/**
	 * Sets the typeof item.
	 *
	 * @param typeofItem the new typeof item
	 */
	public void setTypeofItem(Type typeofItem) {
		this.typeofItem = typeofItem;
	}

	/**
	 * Gets the local tax.
	 *
	 * @return the local tax
	 */
	public BigDecimal getLocalTax() {
		return localTax;
	}

	/**
	 * Sets the local tax.
	 *
	 * @param localTax the new local tax
	 */
	public void setLocalTax(BigDecimal localTax) {
		this.localTax = localTax;
	}

}
