/**
 * 
 */
package com.mvel.helper;

import java.io.Serializable;

/**
 * The Class Hotel.
 *
 * @author shailendra singh
 */
public class Hotel implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3840036230120935374L;

	/** The name. */
	private String name;
	
	/** The from. */
	private String country;
    
	/** The to. */
	private String city;

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Sets the country.
	 *
	 * @param country the new country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city.
	 *
	 * @param city the new city
	 */
	public void setCity(String city) {
		this.city = city;
	}

}
