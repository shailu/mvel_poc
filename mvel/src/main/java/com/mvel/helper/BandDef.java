/**
 * 
 */
package com.mvel.helper;

/**
 * The Class BandDef.
 *
 * @author shailendra singh
 */
public class BandDef {

	/** The band. */
	private String band;
	
	/** The flight. */
	private Flight flight;
	
	/** The flight max value. */
	private int flightMaxValue;
	
	/** The hotel. */
	private Hotel hotel;
	
	/** The hotel max value. */
	private int hotelMaxValue;

	/**
	 * Gets the band.
	 *
	 * @return the band
	 */
	public String getBand() {
		return band;
	}

	/**
	 * Sets the band.
	 *
	 * @param band the new band
	 */
	public void setBand(String band) {
		this.band = band;
	}

	/**
	 * Gets the flight.
	 *
	 * @return the flight
	 */
	public Flight getFlight() {
		return flight;
	}

	/**
	 * Sets the flight.
	 *
	 * @param flight the new flight
	 */
	public void setFlight(Flight flight) {
		this.flight = flight;
	}

	/**
	 * Gets the flight max value.
	 *
	 * @return the flight max value
	 */
	public int getFlightMaxValue() {
		return flightMaxValue;
	}

	/**
	 * Sets the flight max value.
	 *
	 * @param flightMaxValue the new flight max value
	 */
	public void setFlightMaxValue(int flightMaxValue) {
		this.flightMaxValue = flightMaxValue;
	}

	/**
	 * Gets the hotel.
	 *
	 * @return the hotel
	 */
	public Hotel getHotel() {
		return hotel;
	}

	/**
	 * Sets the hotel.
	 *
	 * @param hotel the new hotel
	 */
	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	/**
	 * Gets the hotel max value.
	 *
	 * @return the hotel max value
	 */
	public int getHotelMaxValue() {
		return hotelMaxValue;
	}

	/**
	 * Sets the hotel max value.
	 *
	 * @param hotelMaxValue the new hotel max value
	 */
	public void setHotelMaxValue(int hotelMaxValue) {
		this.hotelMaxValue = hotelMaxValue;
	}

}
